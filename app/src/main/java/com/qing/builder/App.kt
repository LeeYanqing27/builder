package com.qing.builder

import android.app.Application
import com.qing.builder.ui.LoggerTree
import timber.log.Timber

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/8/20.
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        application = this
        Timber.plant(LoggerTree())
    }

    companion object{
        lateinit var application: Application
    }
}