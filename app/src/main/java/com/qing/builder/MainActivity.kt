package com.qing.builder

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.qing.builder.databinding.ActivityMainBinding
import com.qing.builder.utils.WidgetUtils
import com.qing.builder.viewmodel.ConnectWay
import com.qing.builder.viewmodel.HomeViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var isShowConnectWay = false
    private lateinit var viewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()
        initView()
        setContentView(binding.root)
    }

    private fun initData() {
        binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
        viewModel = ViewModelProvider(this)[HomeViewModel::class.java]
        lifecycleScope.launchWhenCreated {
            viewModel.theConnectWay.collect {
                withContext(Dispatchers.IO){
                    binding.apply {
                        tvWifiConnect.text = if (it == ConnectWay.WIFI) {
                            "Wifi连接"
                        } else {
                            "网口连接"
                        }
                        WidgetUtils.getInstance().dismissLoading()
                    }
                }
            }
        }
    }

    private fun initView() {
        binding.apply {
            flWifiConnectButton.setOnClickListener {
                isShowConnectWay = !isShowConnectWay
                llConnectWay.visibility = if (isShowConnectWay) View.VISIBLE else View.GONE
            }

            tvWifiConnectWay.setOnClickListener {
                changeConnectWay(ConnectWay.WIFI)
            }
            tvPortConnectWay.setOnClickListener {
                changeConnectWay(ConnectWay.NET_PORT)
            }
            root.setOnClickListener {
                isShowConnectWay = false
                binding.llConnectWay.visibility = if (isShowConnectWay) View.VISIBLE else View.GONE
            }
        }
    }

    private fun changeConnectWay(connectWay: ConnectWay) {
        if (connectWay != viewModel.theConnectWay.value){
            WidgetUtils.getInstance().showLoading(this@MainActivity, "切换中")
            viewModel.changeConnectWifi(connectWay)
        }
        isShowConnectWay = false
        binding.llConnectWay.visibility = if (isShowConnectWay) View.VISIBLE else View.GONE
    }
}