package com.qing.builder.base

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/9/1.
 */
abstract class BaseActivity<VB : ViewBinding> : AppCompatActivity() {

    protected open var mBinding: VB? = null

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        setContentView(mBinding?.root)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
        initialize()
    }

    abstract fun initialize()

    abstract fun getViewBinding(): VB?

    override fun onDestroy() {
        super.onDestroy()
        mBinding = null
    }
}