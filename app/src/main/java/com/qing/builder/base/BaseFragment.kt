package com.qing.builder.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/9/1.
 */
abstract class BaseFragment<VB : ViewBinding> : Fragment() {
    protected open var binding: VB? = null
    protected open val mBinding get() = binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = getViewBinding()
        return binding?.root
    }

    abstract fun getViewBinding(): VB?

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    abstract fun initialize()

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }
}