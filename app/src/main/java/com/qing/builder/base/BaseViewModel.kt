package com.qing.builder.base

import android.annotation.SuppressLint
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelStore
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.Exception
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/9/1.
 */

typealias Block<T> = suspend (CoroutineScope) -> T
typealias Error = suspend (Exception) -> Unit
typealias Cancel = suspend (Exception) -> Unit

open class BaseViewModel : ViewModel() {
    val TAG = "BaseViewModel"
    val needLogin = MutableLiveData<Boolean>().apply { value = false }

    protected fun launch(
        block: Block<Unit>,
        error: Error? = null,
        cancel: Error? = null,
        showErrorToast: Boolean = true
    ): Job {
        return viewModelScope.launch {
            try {
                block.invoke(this)
            } catch (e: Exception) {
                when (e) {
                    is CancellationException -> {
                        cancel?.invoke(e)
                    }
                    else -> {
                        onError(e, showErrorToast)
                        error?.invoke(e)
                    }
                }

            }
        }
    }

    /**
     * 统一处理错误
     * @param e 异常
     * @param showErrorToast 是否显示错误吐司
     */
    @SuppressLint("WrongConstant", "ShowToast")
    private fun onError(e: Exception, showErrorToast: Boolean) {
//        when (e) {
//            is ApiException -> {
//                when (e.cause) {
//                    -1001 -> {
//                        if (showErrorToast) {
//                            Toast.makeText(AppHelper.mContext, e.message, 1000).show()
//                        }
//                        needLogin.value = true
//                    }
//                    // 其他错误
//                    else -> {
//                        if (showErrorToast) Toast.makeText(AppHelper.mContext, e.message, 1000)
//                            .show()
//
//                    }
//                }
//                Timber.tag(TAG).e(e.toString())
//            }
//            // 网络请求失败
//            is ConnectException, is SocketTimeoutException, is UnknownHostException, is HttpException -> {
//                if (showErrorToast) Toast.makeText(AppHelper.mContext, "网络请求失败", 1000).show()
//
//                Timber.tag(TAG).e("网络请求失败%s", e.toString())
//
//
//            }
//            // 数据解析错误
//            is JsonParseException -> {
//                Log.e(TAG,"数据解析错误"+e.toString())
//
//            }
//            // 其他错误
//            else -> {
//                Log.e(TAG,"其他错误"+e.toString())
//
//            }
//
//        }
    }

}
