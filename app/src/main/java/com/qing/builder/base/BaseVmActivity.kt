package com.qing.builder.base

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/9/1.
 */
abstract class BaseVmActivity<VB : ViewBinding, VM : BaseViewModel> : BaseActivity<VB>() {
    protected open lateinit var mViewModel: VM

    //加载数量
    protected open val mTotalCount = 20
    protected open var mCurrentSize = 0//当前加载数量
    protected open var mCurrentPage = 0//当前加载页数
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        observer()
        initView()
        initData()
        setListener()
    }

    open fun setListener() {
    }

    open fun initData() {

    }

    open fun initView() {

    }


    /**
     * 订阅退出登录逻辑
     */
    private fun observer() {
        mViewModel.needLogin.observe(this) {
            //如果未登录，跳转到登录页面

        }
    }

    private fun initViewModel() {
        mViewModel = ViewModelProvider(this).get(viewModelClass())
    }

    abstract fun viewModelClass(): Class<VM>

    override fun onDestroy() {
        super.onDestroy()
        mCurrentSize = 0
        mCurrentPage = 0
    }
}