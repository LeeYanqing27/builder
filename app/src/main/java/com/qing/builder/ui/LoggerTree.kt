package com.qing.builder.ui

import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.FormatStrategy
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import timber.log.Timber

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/8/20.
 * 日志输出工具类
 *
 * @param usePretty 是否使用格式化打印
 */
class LoggerTree(private var isUserPretty: Boolean = true) : Timber.DebugTree() {
    init {
        if (isUserPretty) {
            val formatStrategy: FormatStrategy = PrettyFormatStrategy.newBuilder()
                .methodCount(1)
                .showThreadInfo(false)
                .tag("LoggerTree")
                .methodOffset(4)
                .build()
            Logger.addLogAdapter(AndroidLogAdapter(formatStrategy))
        }
    }

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        super.log(priority, tag, message, t)
        if (isUserPretty) {
            Logger.log(priority, tag, message, t)
        } else {
            super.log(priority, tag, message, t)
        }
    }
}