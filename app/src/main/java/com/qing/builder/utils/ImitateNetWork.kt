package com.qing.builder.utils

import com.qing.builder.viewmodel.ConnectWay

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/9/3.
 */
object ImitateNetWork {

    /**
     * 切换连接方式
     */
    fun changeConnectWay(connectWay: ConnectWay): Boolean {
        Thread.sleep(2000)
        return true
    }
}