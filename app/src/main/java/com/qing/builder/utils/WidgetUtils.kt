package com.qing.builder.utils

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.KeyEvent
import com.qing.builder.R
import com.qing.builder.widget.LoadingToast


/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/9/3.
 */
class WidgetUtils {

    private var alertDialog: AlertDialog? = null

    companion object {

        private var instance: WidgetUtils? = null

        @JvmStatic
        @Synchronized
        fun getInstance(): WidgetUtils {
            if (instance == null) {
                instance = WidgetUtils()
            }
            return instance as WidgetUtils
        }
    }

    fun showLoading(context: Context, loadingText: String) {
        val view = LoadingToast(context)
        alertDialog = AlertDialog.Builder(context,R.style.TransparentDialog).create().apply {
            setView(view)
        }
        alertDialog?.apply {
            setCancelable(false)
            setOnKeyListener(DialogInterface.OnKeyListener { _, keyCode, _ -> keyCode == KeyEvent.KEYCODE_SEARCH || keyCode == KeyEvent.KEYCODE_BACK })
            show()
            val l2 = window?.attributes?.apply {
                width = context.resources.getDimensionPixelSize(R.dimen.loading_dialog_weight)
                height = context.resources.getDimensionPixelSize(R.dimen.loading_dialog_height)
            }
            window?.attributes = l2
            setCanceledOnTouchOutside(true)
        }
    }

    fun dismissLoading() {
        alertDialog?.dismiss()
    }
}