package com.qing.builder.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.qing.builder.base.BaseViewModel
import com.qing.builder.utils.ImitateNetWork
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/9/3.
 */

enum class ConnectWay{
    WIFI,NET_PORT
}

class HomeViewModel : BaseViewModel() {

    //连接方式
    private val connectWayFlow = MutableStateFlow(ConnectWay.WIFI)
    var theConnectWay: StateFlow<ConnectWay> = connectWayFlow

    fun changeConnectWifi(connectWay:ConnectWay) {
        viewModelScope.launch {
            if (ImitateNetWork.changeConnectWay(connectWay)) {
                connectWayFlow.emit(connectWay)
            }
        }
    }
}