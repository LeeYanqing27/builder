package com.qing.builder.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.widget.LinearLayout
import android.widget.TextView
import com.qing.builder.R
import com.qing.builder.databinding.HomeFragmentLoadingBinding

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/9/3.
 */
class LoadingToast : LinearLayout {

    constructor(context: Context):super(context)

    constructor(context: Context,attributeSet: AttributeSet):super(context,attributeSet)

    constructor(context: Context,attributeSet: AttributeSet,defStyleAttr:Int):super(context,attributeSet,defStyleAttr)

    private lateinit var view: View


    var loadingText = ""



    init {
        view = LayoutInflater.from(context).inflate(R.layout.home_fragment_loading, this)
        initView()
    }

    private fun initView() {
        (view.findViewById(R.id.tv_loading_text) as TextView).text = loadingText
        val rotateAnimation1 =
            RotateAnimation(0F, 360F, Animation.RELATIVE_TO_SELF,0.5f, Animation.RELATIVE_TO_SELF, 0.5f
            )
        rotateAnimation1.apply {
            fillAfter = true
            duration = 800
            repeatCount = 800
        }
        (view.findViewById<View>(R.id.icon_loading)).startAnimation(rotateAnimation1)
    }
}